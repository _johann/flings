#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2019-04-12 Fri 22:11]--[2019-04-12 Fri 22:36] =>  0:25
     :END:

     He solemnly got up from his office chair. There was a mindless shutting of his laptop, smothering the only source of light.

     He sat in the brisk dumbfoundment of darkness, darkness that was pierced by the miniscule decrees of the lighting of kitchenware. It wasn't so much a silence as a clear opportunity to peak into the befuddled confoundment that was his attention.

     He sighed and began padding his pockets for his cell phone. He used it as a flashlight to aide him in his treacherous journey to his bathroom, through the kitchen that still had errant pieces of shattered glass from the hot sauce bottle he had accidentally knocked over weeks or months ago.

     He had been reading about the great lore of evolution and in that context his reflection baffled him. What was all this?

     He was perhaps too unremittant on his savoring the extempore over the contemporary. His life was a series of unrepentant heaps and flatland hurdles, laid out in grooves of whatever behooved him for a span of two weeks or so.

     There was an effort, however poorly embodied, to shape out the heaps and hurdles into something more /evolutionary/ or atleast evolutionary-adjacent or aware.

     Was a sentient beings journey through the wilderness a struggle enough to contribute to some general concression of the struggle and ecstacy of being? On that night, the answer was certainly 'please don't make me answer that right now.'

     As it would be and turn to be. He had started getting the hunch of what was forming the chaotic grammar of his consecutive tour of the wilderness was the pinched loaf scurrying that would come via maybe one or two many days of disenchantment.

     The hope of panacea of directionaltiy and purpose for the next 40 (if lucky) years of his life to be found in some constellation of a series of promising sounding things to read that he would lift from the internet would eventually turn up stale or problematic.

     Mentally it wasn't so much a wilderness as much as it was an oceanic sprawl of resonances and phantom limbs. That was the only thing that compelled him unto the againness, the pull back in wasn't so much a mere foolishness.

     It was, as much as it could warrant to make the distinction, a /sincere/ foolishness. These days were the sort of offseasons that made him curious to the amply declared conclusions of deconstructing all grand narratives and demurring all these potentially aeonic bearing whys and wherefores into a stalemate.

     He knew it was ridiculous. There was too much potential talk of progress to betray it categorically. And yet it still called out to him, like a siren beneath the sea itself. And he knew what would happen if he took /that/ dive.

     He would surface on some new en-media-res island of passion and begin a new adventure and voyage with premises too erotically charged to dare refuse until, well, they were not.

     What was all this?

     That was the humming enquiry that exhausted him as he brushed his teeth. On good days, there was a culmination of weddings and consummations that compelled his heart to not just beat, but beat exceedingly so, if not sing.

     When reason was refused acceptance into the phenomena itself, it could become a daunting, cryptic idol. On good days, ones of less defeat, his being could sustain its pulse into a fuller verticality of moment to moment, where logic would prehend itself in symphonic tapestries of feeling attention.

     He did not want to surrender into another lapsed defeated dream, not without himself being the pallbearer and recidivist book keeper. Paths must be remembered, if not exhausted, if not brought to a map, and reminded of their frothy emergence being not quite the territory.

     He thought about doing a stretch before bed, but instead was content to reach for the desperately difficult to scratch area right below his neck to scratch, which was stretching enough.

     What was there to procure tomorrow? He, having no leads and having no real ignition to find any, was content to just shelf the question with a tacit prayer that day break could compel him to a rumor of progress.

     There were brief, fleeting moments of declarations of how strange it was. And by 'it' he would plant a pronoun for the antecedent he was desperately trying to make introductions with--holies of holies, entirities of entirities.


   Entered on [2019-04-12 Fri 22:10]

     