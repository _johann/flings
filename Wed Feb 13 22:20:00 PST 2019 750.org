#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2019-02-13 Wed 22:53]--[2019-02-13 Wed 23:18] =>  0:25
     CLOCK: [2019-02-13 Wed 22:21]--[2019-02-13 Wed 22:46] =>  0:25
     :END:

     It never seemed to meet in full greeting. The edges wained and dwindled in some strained hypothetical slightly beyond the range of the themselves that had eachother.

     When had the courage to quiet all the clamoring gestures, trite undulations of closed loop doomsdays, hyperironic narrative structures deadpanning to a clamorous self-interrogating amnesia, the horror of falling seemed, in an existential tantra, boil over into a pressure into his forehead.

     It wasn't a matter of trusting it. It was a lower current of being than trust, it was a knotted musculature too dimwitted to know an entirity of relief. That seizing compulsion to rudimentary here-now's were so absorped in their starvation as to not transmute themselves to the great sacrificial swoon of being.

     If conclusion would subdue itself as a tin lid over the head and alchemize into a mercurial surrender into all-and-All...

     There were moments of stark nude confrontation of the whole here-now occasion. Whole bodily armistice in a riddle adorn obvious--a chrystalline persuasion permeating up and through and below and beyond all formalities and mental enterprises.

     And when he would lean in even further, he could see the whole inverted inhalation--a molten molting of appearances and duration as a characteristic that could rest in the palm of the hand.

     But suddenly! A seizing! And the return of the genuflected existence yielding to the syncopated clapperboard enaction of the mortal romantic overture and adventure. To the point the entire experience prior was remembered as such, a mere experience, rather than an impression of the Divine radiance to which all experience was a mere apparent modification.

     Somehow, in the monotony of a rosary beaded continuity of en media res happenstance, there was some geist of raw cognitive fruition, creating an enquiry so emptied as to become destined for its own eureka, that entire question of isness, mirrored on to itself in sincere renunciations of all lesser than All...

     So delicate as to quickly reverse its polarity, a governing operandi lost to the edges of event horizons, a secret creator-god in all mundane and monotonous persausions, speerheading congealments of zodiacal movements of printing press psychological absences--an ensemble cast of forgetful subpersonalities achieving the helm of self for incorrigible expressions of unnamed, insatiable utterances of I am.

     A march of fugue permutations, reminisce and accidental, each usurping eachothers sublimated horizon with no upward swoon of possibility, but an aggregious and trecherous momentum of desperation and annihilation.

     None could interrogate as to verify one another as much as declare themselves absolute until vanished. Gone! Some providential stance of matter's spirit...

     The here-now greeting of an ever-so compliant nervous system, manifold blossoms upon blossoms within grave and barren, so compelled by the proclivities of its very instantation, so subtle as to be present to the earnest enquirer, but not overbearing as to never be achieved without a humble surrender.

     Pulses, their orbits, rumors of ascension and third days, organic cyphers upon interiors and exteriors, and the sordid confession of the uncouth and unseemly heaven on earth.

     Heaven on earth? How can ever such a thing be? Surely it is better to say that the earth as rested in the radiance of heaven and has always done so! The earth, of the heavens, in its Lawful place in illusive undulations of becoming, the great outshining of all dimwitted attempts of tracing the always-already with mum puppetry and pantomime.

     Unison so gentle to naivety as to awaken gleefully from it, without dilemma and fear. Shakti through the fourth wall, a secret embrace of apprender, apprending, and apprehended (unveiled).

     All embraced and realized upon the pyre of itself. What to make of a sense of sequence? We can deny its entirity and presume a transcendent stance, the end was the end and the rest was ill-measured or we can yield to the fullest implcation of /is/, the fullest acknowledgement and addressing of /being alive/ in its successive appraisals of now, a whole body prayer for our own reclaiming of the heavens of the earth.

      The spiritual and the transcendent arise in the loving realization of Divine radiance. I am the greeting of the heavens of the earth.
   Entered on [2019-02-13 Wed 22:20]

     