#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-04-20 Fri 18:10]--[2018-04-20 Fri 18:35] =>  0:25
     :END:

     Pattern recognition. Life yields to it, regardless to a priori cathedrals and blurry images of deities and saints.

     Patches of grass, to their own seizing, rise from the human decor of gravel. The hillside had a train track hemmed to it, and with a train track came a ditch, with all the little human utilitarian delighs.

     "You are free, now. What's stopping you?" she rocked back and forth. Her physicality was all wrenched up, in disbelief of her own full posture. The procession of the train--in its clattered, dull steel chattering--swallowed the panting of her shallow breaths.

     She took the tea bag out of her cup of tea and writhed it in her hand as she stared out the window. The stained current dripped down her forearm, zigzagging to the rhythm and locomotion, eventually dropping down on her well pressed tan slacks.

     "It is hard to escape the pressure of emptiness..." she whispered quietly as she squeezed the tea bag harder, causing it to burst in her hand. Her panic stricken behavior shunted, she opened her hand perplexed and disgustedly threw its tattered state back into the tea glass.

     Her tea, rather lessened from its comfort, became a motion of her own indecision. The motion threatened her, contracting her body even further, snapping her own consciousness into a lapse into another impulse, then another, then another. Until she passed out.