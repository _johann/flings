#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-05-01 Tue 20:22]--[2018-05-01 Tue 20:47] =>  0:25
     :END:

     He felt the eyes flock to him. His grace and movement was undeniable, yet it was hard for him to not release the fantasy that it was him with the constant efflux of naturality.

     When he was younger, he had, out of a fit and out of rage, shoved him out of the way to make the winning goal. He had shattered his wrist and was out of commission for almost two summers. He apologized constantly, explaining that Ralph had pushed him and he incidently used him to regain his footing.

     The guilt ate the lining of being--the wild reaction to the way spectators were quietly seduced into a devotion. He felt the vacuum, the suspense, the anticipation and he couldn't deny a monsterous reaction to steal his brother's throne.

     Everyone commemerated the love between them--out of a panicked guilt, he would serve on hand and foot for his impaled brother. Anything he needed--his missed assignments from school, a motivational quote from Gretzkey, his recapping of the state of his middle school classes drama...


     "Jerri keeps on getting asked out by Tom. For some reason, just because you aren't around he thinks he can keep on making moves."

     "It's fine--I mean, he has a reason for that ridiculousness right?"

     "But he's your best friend."

     Matesh sighed.

     "I know, Ansh. Sometimes people we care about go clueless."

     Ansh brought the plate with milk and cookies to Matesh's beside.

     "Good for your bones, brother!"

     Matesh smiled.

     "Why won't you do anything about it?"

     Matesh had a pained expression that moved through the left to right side of his body, causing his wrist to writhe and rustle in the sheets.

     Matesh collected himself from the seizing pain and smiled through his wince.

     "For the same reason I rest to let my wrist heal. Better to wait till people are ready to hear you than to waste your breath. Right Ansh?"

     Ansh's breath stuttered in the way he would chubbily chortle when he felt overwhelmed. He turned a pale color and gulped comically, as if he intended to be out of a vaudeville film.

     Matesh laughed as hard as his body would let him.

     "What's the matter, Ansh? Remembering you have pre-algebra homework?"

     Ansh stood speechless. He did forget he had pre-algebra homework.

     "Don't worry, Ansh. I can help you after I take a 15 minute nap. I'm exhausted."

     He pointed charmingly to the door with his right hand.

     "You'll get the hang of it buddy--no need to have the bejeezus spooked out of you HA..."

     His laughter was woven with intermitten wincing--he found it all too funy to be dampened by the pain that touched the gag.

     "HA euah HA... Thanks again for the snacks, bro."


     Ansh clumsily exited the room, first backing away with his eyes still locked in his brothers, then a sheepish scurrying to his own room.

     He paced the edges of his Buzz Lightyear rug, occassionally gatching his own narcissistic gaze in his dressing closet mirror. Did he know he pushed him? Has he been secretly mad at him this whole time? Why would he do that to his brother? Why would Tom?

     How could he ever make this right with his brother? By trying to figure that sohcahtoa thing by himself?

     He would get more sleep. He'd be more well rested. That'd make up for my betray. He mouthed the words in the mirror with a tear in his eye.

     He sat in the corner of his room, rocking back and forth, ignoring his brothers calls for him while muting his solemn sorrow.


   Entered on [2018-05-01 Tue 20:21]

     