#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-04-19 Thu 21:06]--[2018-04-19 Thu 21:31] =>  0:25
     :END:
     He had been struggling to open the jar for about three days. The thought of going outside and being susceptible to the locusts kept him in the fog of war.

No, he would not put himself at risk. He would pantomime the gesture of opening the jar. The jar would be opened once the gesture had its throughness. Given enough, it would be done. And the dilemma would reside to a creaturely absence of mind.

And the natural processions would march along without him, as his own stranded nature would leech inward on itself, pantomining gestures in a sollipsistic echolalia.

"What I am saying is." He couldn't help but remind himself of himself when he talked to others. He had become such a leper, so weary of his own reflection in the upright longitude of consciousness he desperately sought his own lateral motions--chasing his own cheap thrill thoughts.

Too in awe of any start whatsoever, he was unashamed voyeur of the false. Continuity demanded a notion of orbit--something greater that steered his sense of immediacy. But he opted to be an oaf to the present, never having the where-with-all to stoke the coals of intuition in the privacy of his own mind.

He was both desperate and detached--desperate for the mental image, detached from the consequent. Whiplash gestures of amnesia and denial fray the edges of his own recollection of, well, anything.

"I hear you. You don't need to..."

His cousin, on the other end of a land line, was high and staring at the way the light refracted on his kitchen top soap bubble. The conversation was less out of sympathy and more out of a neurotic pre-emptive guilt-riddenness if this person were to kill himself.

He thought it over. He had given the same speech last week. Maybe this was just it--an inertial frame of reference that simply cannot see the irony. A mollusk unto impulses, daft to conceptual engagement, who enjoys calling his cousin about how he still hasn't open the jar.

The digitization of his essence was well and good. A budgeted, reserved account of his possibility missed the strange inflections--lapses of brilliance, like a beautiful song out of a speeding car, never having its proper grounding.

The fog of war was, if anything, more rigidly aligned with the parts of himself that, well, hurt the least intensely. Even if the intense flight of death were to strike for only an instance, yielding to the promise of new being--like he would be a jar, opened. Or a bubble bursted.

He'd rather stay cool in the shadow that strangled him more rhythmicly to his own anxieties, muttering half baked nonsense, fumbling gestures, contributing CO2.

"What I am saying is I don't /need/ to have this jar opened. It is my own practice, an exercise. And who are you to..?"

"I didn't say anything about the jar this conversation. You are talking at a memory."

"I knew that."
   Entered on [2018-04-19 Thu 21:02]

     