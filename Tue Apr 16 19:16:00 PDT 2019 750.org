#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2019-04-16 Tue 19:16]--[2019-04-17 Wed 07:51] => 12:35
     :END:

 #+BEGIN_VERSE
 ecstatic confessions
between zero
and one

vague veined coarsing
stand-in nomenclature

in heaven's departure
or its semblance
or its unfurling
or its playful forgeting

the incorrigible mirror
made in peculiar figure
supplanting drive
in fatigue

who remembers
the first thought
each day

?

who collides
with the shadow
of their maker

?

is there
such a thing
as innocence
in the whirlwind
of forevers
impetus

?

the incorrigible contrasting
of a self-defeating
recollection
of someone elsewhere
somehow better

than the right-here
in some relation
to now
chasing, maybe,

out of breath
rumors in mind

how can i
become anything
if i am constantly taking
a fire hydrant defeatism
on any trance that looms me
in a charm of Goddess

?
 #+END_VERSE

 It took some time but it had finally happened. In the dreary predilictions of day and night, in forthright fulfillment of sticky-noted prophecy, he had become tired of himself.

 He implored his innards for that gentleness of feeling that his past was rumored to have, if he were to trust that breeze.

 The cool impression of the glass of water subsisted somewhere in the edges of his awareness. He gave it an unseemingly grip, perhaps of the strange seizing his body would be under when he would take his uppers.

 These uppers were an absolute must to fulfill the legion of responsibilities to satisfy the rubric of not getting fired. Modafinil, cups of coffee, copious amounts of water, headphones to drown out any daggar'ed triggering from coworkers.

#+BEGIN_VERSE
when will
you know
the right hand
of the right time
of the right now

this resistance
to innocence
pressed unfairly
into the dithering
interpretation
that promotes the stage
of premeditated mirroring
diffusing all light
demuring all nature-sense
to a drowned out muzzle
of best
of all possible
confessions

of the one true anything
fatigued in its own sens
of planned absolescence

no, there is something more
this is the unmediated expanse
confessing itself
however bashful
however weary
of all the learned grooves
and caveats
necessary
to make home of being
rather than rumormill

i want to be heard
heard in the quiet fortitude
of the mystery of being alive

i don't want to collapse
into childhood at
these vibrant slingshot vicissitudes
and revivals of an unkept past

i do not ask for mercy
from life itself
it cannot hear me
in that sort of tone

i ask for her
to spare of me
nothing

i am here, afterall,
so why not taste the whole entirity
of impetus
of the churned concern
of habitus
#+END_VERSE

His coworker would always talk about what his dreams looked like. He had not remembered a dream since he found it necessary to take the job and sign a covenant with himself to not get fired. Everything felt like it was happening under alarm.

His body sort of lurched forward in exhaustion, making him spend most of his weekends bedridden and avoidant any and all pleasantries. There was a moment in his youth when he claimed he would have never wanted to be a professional athlete.

He imagined that he would just get tired, doing the same thing day after day. Who would want to do that? Now, he thought of the athlete's prison longingly, each creaturely arrangement purposeful and headed under some entirely consummate notion of /goal/ and progress.

No such flow and feed was at this place. It was all secret covenes of who was so fortunate to be cc'ed in email threads and pinged in siloed excursions in the work chat.

There was a whole game, in and of itself, to understand the game of decisioneering himself. In truth, after the first week, he deemed it a game not worth playing. His main concern, more than anything else, was simply not to get canned.

Horde money and not get canned. Simple enough of an injunction, more than anything else.

But it made him estranged upon his normal estrangement, to the point where he would almost shed a tear at the sentimency of beer commercials, showing people hanging out and enjoying their company shook him to the bones of what it was he could even connect to, anymore.

#+BEGIN_VERSE
connection
as mental activity
is quoted unquoted

is not connection
it is,
in so much
that everything is
connected,
but it is not
freedom
of the boundary
at the helm

do you understand?
#+END_VERSE
   Entered on [2019-04-16 Tue 19:16]

     