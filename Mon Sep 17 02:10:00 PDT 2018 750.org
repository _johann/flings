#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-09-17 Mon 02:10]--[2018-09-17 Mon 02:35] =>  0:25
     :END:

     She felt the obligation. It pressed down into her temples and made her squirm at her desk. The great supposition denied the right of way at her throat--the constriction inflected so sternly and uniformly that she would forget what it was like to know its release.

     She looked nervously at the digital clock she had set underneath her desk as to avoid looking at it nervously.

     She prayed for the means to hold a whole universe in her head and to speak it into being.

     How do you collapse a waking dream? Should it be collapsed or further explored? Released into the life-as-life? Avoided under a sheath of its purpose?

     She rocked back and forth in her chair while tapping her right foot against the table leg. The pencil stomped and rolled toward the binding of her notebook.

     She forgot she had a cup of water. It was slightly to the right, just out of frame.

     Pouncing on the pencil, she wrote in a starved hunger...

     "He did not know how to annihilate his desire."

     "His desire, consuming him thus, began to annihilate him."

     She titled back in her chair and took a deep breath.

     "A trance of surrendered recognition, seduced by the heavy handed fantasy and the utter refusal to ever consider its probability..."

     She lept out her chair and began to mutter to herself.

     "Possessed!"

     She put her right fist into her palm.

     She sprinted from side to side in her room. She jumped up and down. She took a giant step up and down her chair, leading with her right leg first, then her left.

     She walked briskly to her kitchennete sink and splashed water in her face. She placed her face in the freezer. She wafted her hand above the stove like a trapeze artist.

     She grabbed a banana in a fruit basket on the counter and mumbled to herself.

     "Awake!"

     She marched to a safe in her closet and punched in the combination. She pulled out a duffle bag--scrummaging through it, she grabbed a pencil box. In the box was a series of tiny plastic baggies.

     She swatted at the ground behind her. Finding the yellow legal pad, she brought it before her to remember what her dose was yesterday.

     Her theory was the nervous system gave negative returns for fixed hits of single trends. You can get the same high and bypass the whole blaiseness of addiction by having a rich variety of /kinds/ of drugs and rotate.

     "FoooocococccuS!" she shouted while dropping the drugs from her hand.

     She sprinted back to her desk.

     "He lept into doomed traps of vigilant, wasted hope. It wasn't a sacrifice, because it wasn't conscious. It was an incidental wasting /away/ and /away/ and /away/ in a biodegradable affair of nebulous purpose."

     She clapped her two hands together. She waddled to the sink and attempted to drink straight from it. Hand to her chest, she began to suddenly go short of breath.

     Her breath grew more constricted. The suffocation fastened the rhythm. In a shock, she starkly walked toward her window. The latches were involved, but she did manage to pry it open. She insisted her face against the filthy window screen and breathed as deeply as she could.

     The filth made her cough and seize in an enormous barricade of hysterical sensation. She dropped to the ground in an attempted retreat, moaning, sneezing, crying, yelling--every possible utterance at once.

     She pushed herself off the floor and stumbled back to her freezer, where she rested her head while pounding her chest.

    "RESILLIEIEEENNT" she said, stomping her feet on the ground.

    She sat bow legged on her kitchen floor with her eyes closed. The joy of madness was the option to not only forgo concern, but disintegrate and dissolve and disassociate from necessary fellowships warranting pain.

    "I didn't say goodbye. I didn't say goodbye... he's gone. He's gone. He's GOOONE."

    She rocked back and forth, in a heated spiritual plight to take on the death directly or to stifle her annihilation for another scene, another moment, another lifetime beyond now.

    When she cried, her belly felt like a basin for an enormous swell of emotions that rushed through her heart and throat. The ferocity of her wailing and her tears was a release of a request for peace.
   Entered on [2018-09-17 Mon 02:10]