#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-05-30 Wed 21:48]--[2018-05-30 Wed 22:13] =>  0:25
     :END:

He found it difficult to round up the last ten percent of proclaimation of the finality of the project. The finish line admonished his haunted images of oughts despite barbed measurements.

He was a moron, but only insomuch he fumbled the batton of ambition. Other morons refused to carry it at all--or remember when they had it, what they did with it--poof, at the summonce of an entitled ego, the whole frame of reference would disassociate. Fizzled out of the conscious equation, at the speed of accountability, dodging the shiva'ed eyes of a second person.

Goal? What goal? What would ever make you think that there was a /goal/ to be had in any of this savagely delicate inertia?

He took a stiff sip of coffee from a plain maroon mug. He had no idea where it came from--one day, it was the last mug available. There it was. He placed it on the makeshift coaster that was a stack of napkins he yanked from the fastfood place.

The email was to announce a final draft of the screenplay was ready to be sent. He couldn't write it at the computer because any little facet of the outside world would stab his soul in a pang of undecided surrender.

He had worked for naught while Jerry achieved such and such! Posing so delightfully before delightful things while he...

And he couldn't trust himself to walk to the store to get paper. He didn't have the resources to panick buy a sublimating amount of food so that he may cower away from his feelings.

So he scowered the ground of his damned studio floor for scraps of anything remotely capable of serving as parchment. He didn't know any other way to work other than riding fits of delirium.

Big long swoons of lavender stretches of time bellowed through his whole body. No frayed edges of disconcernment. No worries what his family would ever think or consider. Just rampant, pulsant inhales of infinity.

When he was out, he would have an anxious concern of whether he had bottled it. And it was out and over--money was dry and every run in he'd ever have with inspiration seemed to lead him to the same terminus. This was it. It was done.

He would have inca dreams of self-sacrifice over a bellowing hell encased in a mountain teeming into the clouds. The coffee, the smooth pursuit of utterances, the shutin nirvana--all to be sacrificed into some mad purveyor in a dumb fit for the full form.

"Don't worry about it being /good/, man. Why would you expect it to be /good/?"

He hung up the phone when peoples console got a little too idyllic. The barbed fabric of the epistemal and ontological was something he found mad folk failed to use to exfoliate the dull logics and habits of their creaturely cowardice.

Heartbreak is no excuse inbred inconclusivity. Failure is in the cards and, yes, that meant ample reason to incinerate the deck but he refused to forget the motivation, even if it led him astray.

"Hey, I revised the script. It's done, now. Please stop sending me notes. I'm on the lower end of the spectrum of a narcissist--I know the plight of a moody artist is one of the priviledged plights. But making my movie, even in the face of other movies, would be a good move for you. Because I didn't stop chasing. Really."

   Entered on [2018-05-30 Wed 21:48]