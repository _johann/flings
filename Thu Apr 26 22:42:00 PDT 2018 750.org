#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>
     :LOGBOOK:
     CLOCK: [2018-04-26 Thu 22:17]--[2018-04-26 Thu 22:42] =>  0:25
     :END:

He rocked in and out of the conversation. Tilting back, he could witness the audible interference of dozens of communications.

Occassionally, the speaker would prowl his eyes for contact and, out of nervousness, he would take a sip of his drink.

As the night progressed, the walls seemed to glow into a deeper amber. Panache decour of beheaded creatures and picturesque wildernesses syncopated the rooms edges.

He stood still in the party's clearing. Again, he'd try to subsume an immediate for some grander image--the adhoc statement of a crowded room, lacking all semantia, leaving him as driftwood.

His smile was earnest, but when drawn in to another gathering he'd only take in surface cues--muffled, muted bare minimum oscillations of contact.

After enough drinks, he accidentally opened up to someone. More so they were there and he could not bare it.

"My dad passed away three months ago."

He waited for the person to stop speaking before he kept going.

"Ever since he died, the whole way I experience things can feel like..."

He did not have the wherewithal to lean into the nowmoments ebbs and flows--everything, upon its perception, was passé.

"It feels like everything is wrapped in saran wrap."

"And I am constantly in this... it's not really disbelief, but it is like I am on time delay, constantly reassuring and narrating how it is I ended up in the scenes that I am..."

He let the person he was talking to clumsily fabricate a reason to leave.